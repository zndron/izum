"use strict";

var
    gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass');

// source and distribution folder
var
    source = 'src/',
    dest = 'dist/';

// jquery source
var jquery = {
    in: './node_modules/jquery/dist/'
};

// magnific-popup source
var magnificpopup = {
    in: './node_modules/magnific-popup/dist/'
};

// font-awesome source
var fontawesome = {
    in: './node_modules/font-awesome/'
};

// isotope-layout source
var isotope = {
    in: './node_modules/isotope-layout/dist/'
};

// bootstrap source
var bootstrap = {
    in: './node_modules/bootstrap/dist/'
};

// Fonts source
var fonts = {
    in: [
        source + 'fonts/*.*',
        bootstrap.in + 'fonts/*.*',
        fontawesome.in + 'fonts/*.*'
    ],
    out: dest + 'fonts/'
};

// JavaScript source
var javascript = {
    in: [
        source + 'js/*',
        bootstrap.in + 'js/bootstrap.js',
        jquery.in + 'jquery.js',
        magnificpopup.in + 'jquery.magnific-popup.js',
        isotope.in + 'isotope.pkgd.js'
    ],
    out: dest + 'js/',
    watch: source + 'js/**/*.js',
};

// css source
var css = {
    in: [
        bootstrap.in + 'css/bootstrap.css',
        fontawesome.in + 'css/font-awesome.css',
        magnificpopup.in + 'magnific-popup.css'
    ],
    out: dest + 'css/',
};

// Images source
var images = {
    in: source + 'img/**/*',
    out: dest + 'img/',
    watch: source + 'img/**/*',
};

// Our scss source folder: .scss files
var scss = {
    in: source + 'scss/main.scss',
    out: dest + 'css/',
    watch: source + 'scss/**/*.scss',
};

// copy fonts to dest
gulp.task('fonts', function () {
    return gulp
        .src(fonts.in)
        .pipe(gulp.dest(fonts.out));
});

// copy javascript to dest
gulp.task('javascript', function () {
    return gulp
        .src(javascript.in)
        .pipe(gulp.dest(javascript.out));
});

// copy css to dest
gulp.task('css', function () {
    return gulp
        .src(css.in)
        .pipe(gulp.dest(css.out));
});

// copy images to dest
gulp.task('images', function() {
    return gulp
        .src(images.in)
        .pipe(gulp.dest(images.out));
});

// compile scss
gulp.task('sass', function () {
    return gulp
        .src(scss.in)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(scss.out));
});

// watch for changes in files
gulp.task('watch', function () {
    gulp.watch(scss.watch, ['sass']);
    gulp.watch(javascript.watch, ['javascript']);
    gulp.watch(images.watch, ['images']);
});

// The default task (called when you run `gulp`)
gulp.task('default', ['sass', 'images', 'javascript', 'css', 'fonts', 'watch']);