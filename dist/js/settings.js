$(document).ready(function () {

    "use strict";

    if ($('.owl-carousel').length) {
        $('.owl-carousel').owlCarousel({
            margin: 10,
            responsiveClass: true,
            nav: false,
            navText: [ '<', '>' ],
            dots: false,
            items: 1,
            autoplay: true,
            autoplayTimeout: 4000,
            autoplayHoverPause: true
        })
    }

    if ($('.gallery, .masonry').length) {
        $('.gallery, .masonry').each(function() { // the containers for all your galleries
            $(this).magnificPopup({
                delegate: 'a', // the selector for gallery item
                type: 'image',
                gallery: {
                    enabled:true
                }
            });
        });
    }

    $(window).load(function(){
        if ($('.masonry').length) {
            $(".masonry").isotope({
                itemSelector: ".selector"
            }), $(function () {
                var t = $(".masonry").isotope({
                        itemSelector: ".item"
                    }),
                    i = {
                        numberGreaterThan50: function () {
                            var t = $(this).find(".number").text();
                            return parseInt(t, 10) > 50
                        },
                        ium: function () {
                            var t = $(this).find(".name").text();
                            return t.match(/ium$/)
                        }
                    };
                $("#filters").on("click", "li", function () {
                    var n = $(this).attr("data-filter");
                    n = i[n] || n, t.isotope({
                        filter: n
                    })
                }), $(".filters").each(function (t, i) {
                    var n = $(i);
                    n.on("click", "li", function () {
                        n.find(".active").removeClass("active"), $(this).addClass("active")
                    })
                })
            })
        }
    })
});
